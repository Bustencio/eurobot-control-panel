const express = require('express');
const path = require('path');

const app = express();

app.use(express.static("../public"));

app.get('/', (req, res) => {
    res.sendFile(path.resolve('../public/index.html'));
});

app.listen(3000, () => {
    console.log('Server running on 192.168.1.53:3000');
});