let ros = new ROSLIB.Ros({
    url: 'ws://192.168.1.45:9090/'
});

ros.on('connection', () => {
    let item = document.getElementById("status")
    item.innerHTML = "Conectado";
    item.style.color = "green";
    window.localStorage.setItem("count",0);

});

ros.on('error', (error) => {
    let item = document.getElementById("status")
    item.innerHTML = "Error";
    item.style.color = "orange";
});

ros.on('close', () => {
    let item = document.getElementById("status")
    item.innerHTML = "Desconectado";
    item.style.color = "red"
    
    // Intentamos reconectar al WebSocket si se pierde la conexion
    let count = window.localStorage.getItem("count");
    if (count < 5){
        count++;
        window.localStorage.setItem("count", count);
        setTimeout(() => {
            window.location.reload(1);
        }, 5000); 
    } else {
        alert("Comprueba que el servidor está funcionando y refresca la página.")
    }
    
});

// Dibujamos el robot en el canvas
const drawRobot = (x, y, rot) => {
    let canvasRobot = document.getElementById("canvasRobot");
    let context = canvasRobot.getContext("2d");
    const robotImg = new Image();
    robotImg.src = 'eurobotRobot.png';
    robotImg.onload = () => {
        context.clearRect(0, 0, 1500,1000);
        context.save();
        context.translate(x*100 ,1000 - y*100);
        context.rotate(-rot);
        context.drawImage(robotImg, -robotImg.width / 2, -robotImg.height / 2);
        context.restore();
    }
}
    

let cmd_vel = new ROSLIB.Topic({
    ros : ros,
    name : '/robot0/cmd_vel',
    messageType : 'geometry_msgs/Twist'
});


// Conexion al topic de velocidad
cmd_vel.subscribe((message) => {
    document.getElementById("topicMain1Val").innerHTML = message.linear.x.toFixed(2);
    document.getElementById("topicMain2Val").innerHTML = message.angular.z.toFixed(2);
    
});

let odom = new ROSLIB.Topic({
    ros : ros,
    name : '/robot0/odom',
    messageType : 'nav_msgs/Odometry'
});

// Conexion al topic de posicion
odom.subscribe((message) => {
    let x = message.pose.pose.position.x.toFixed(2);
    let y = message.pose.pose.position.y.toFixed(2);
    let quatX = message.pose.pose.orientation.x; // Quaternion de orientacion
    let quatY = message.pose.pose.orientation.y;
    let quatZ = message.pose.pose.orientation.z;
    let quatW = message.pose.pose.orientation.w;
    let quat = new THREE.Quaternion(quatX, quatY, quatZ, quatW);
    let rot = new THREE.Euler().setFromQuaternion(quat, 'ZYX');
    let rotFinal = (rot.z * 180)/ Math.PI; // Radianes a Grados 
    document.getElementById("topicMain3Val").innerHTML = "X: " + x + " - Y: " + y ;
    document.getElementById("topicMain4Val").innerHTML = rotFinal.toFixed(2) + "&deg";
    drawRobot(x, y, rot.z); // Pintamos el robot en las coordenadas/orientacion obtenidas
});

